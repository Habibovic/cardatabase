package com.example.korisn.cardatabase;

/**
 * Created by KOrisn on 11.11.2017..
 */
import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;
import com.activeandroid.query.Select;

import java.util.Date;
import java.util.List;

@Table(name = "CarDetails")

public class CarDetails extends Model{
    @Column(name = "Name")
    public String name;
    @Column(name = "ImageUrl")
    public String imageUrl;
    @Column(name = "Price")
    public float price;
    @Column(name = "Description")
    public String description;
    @Column(name = "Email")
    public String email;
    @Column(name = "PublishDate")
    public Date publishDate;

    public static List<CarDetails> getAll() {
        return new Select().from(CarDetails.class).execute();
    }

    public static List<CarDetails> getSpecific(String name) {
        return new Select()
                .from(CarDetails.class)
                .where("Name = ?", name)
                .orderBy("Name ASC")
                .execute();
    }
}

