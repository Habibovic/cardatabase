package com.example.korisn.cardatabase;

import android.animation.Animator;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.activeandroid.ActiveAndroid;


import java.util.Date;
import java.util.List;



public class MainActivity extends AppCompatActivity {

    EditText searchEditText;
    RecyclerView mainCarRecycler;
    ImageView floatingBtn;
    RelativeLayout addNewCarHolder;
    EditText carName, carImage, carPrice, carDescription, carEmail;
    CarRecyclerAdapter mainAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        searchEditText = findViewById(R.id.search_edit_text);
        mainCarRecycler = findViewById(R.id.main_car_recycler);
        floatingBtn = findViewById(R.id.add_floating_btn);
        addNewCarHolder = findViewById(R.id.add_new_car_holder);
        carDescription = findViewById(R.id.car_description);
        carEmail = findViewById(R.id.car_email);
        carPrice = findViewById(R.id.car_price);
        carName = findViewById(R.id.car_name);
        carImage = findViewById(R.id.car_image);

        ActiveAndroid.initialize(this);
        final List<CarDetails> allCarsInDatabase = CarDetails.getAll();
        if (allCarsInDatabase != null && !allCarsInDatabase.isEmpty()) {
            setAdapter(allCarsInDatabase);
        }
        searchEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (charSequence.length() == 0) {
                    if (allCarsInDatabase != null && !allCarsInDatabase.isEmpty()) {
                        setAdapter(allCarsInDatabase);
                    }
                } else {
                    List<CarDetails> searchedCars = CarDetails.getSpecific(charSequence.toString());

                    if (searchedCars != null && !searchedCars.isEmpty()) {
                        setAdapter(searchedCars);
                    }
                }

            }

            @Override
            public void afterTextChanged(Editable editable) {
            }
        });

    }

    public void setAdapter(List<CarDetails> list) {
        mainAdapter = new CarRecyclerAdapter(getApplicationContext(), list);

        mainCarRecycler.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
        mainCarRecycler.setAdapter(mainAdapter);
        mainAdapter.notifyDataSetChanged();

    }

    public void saveNewCar(View view) {
        if (carImage.getText().toString().isEmpty() || carName.getText().toString().isEmpty() || carPrice.getText().toString().isEmpty()) {
            Toast.makeText(getApplicationContext(), "You have to enter all values", Toast.LENGTH_SHORT).show();
            return;
        }
        CarDetails carDetails = new CarDetails();

        carDetails.imageUrl = carImage.getText().toString();
        carDetails.name = carName.getText().toString();
        carDetails.price = Float.parseFloat(carPrice.getText().toString());
        carDetails.email = carEmail.getText().toString();
        carDetails.description = carDescription.getText().toString();
        carDetails.publishDate = new Date();
        carDetails.save();

        List<CarDetails> allCarsInDatabase = CarDetails.getAll();
        if (allCarsInDatabase != null && !allCarsInDatabase.isEmpty()) {
            setAdapter(allCarsInDatabase);
        }

        doAnimation(floatingBtn, addNewCarHolder);
    }

    public void cancelCarInput(View view) {
        doAnimation(floatingBtn, addNewCarHolder);
    }

    public void addNewCarFloatingBtn(View view) {
        doAnimation(addNewCarHolder, floatingBtn);
    }

    public void doAnimation(final View opening, final View closing) {
        opening.setScaleX(0);
        opening.setScaleY(0);
        opening.animate().scaleX(1).scaleY(1).setDuration(500).setListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animator) {
                opening.setVisibility(View.VISIBLE);
                closing.setVisibility(View.GONE);
            }

            @Override
            public void onAnimationEnd(Animator animator) {

            }

            @Override
            public void onAnimationCancel(Animator animator) {

            }

            @Override
            public void onAnimationRepeat(Animator animator) {

            }
        }).start();
    }
}
