package com.example.korisn.cardatabase;

/**
 * Created by KOrisn on 11.11.2017..
 */

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.List;

public class CarRecyclerAdapter extends RecyclerView.Adapter <CarRecyclerAdapter.ViewHolder>{

    List<CarDetails> carDetails;
    Context context;

    public CarRecyclerAdapter(Context context, List<CarDetails> carDetails) {
        this.carDetails = carDetails;
        this.context = context;
    }


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.car_details_custom_row, parent, false);
        return new CarRecyclerAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        CarDetails carDetail = carDetails.get(position);

        holder.title.setText(carDetail.name);
        holder.price.setText(String.valueOf(carDetail.price));
        if (carDetail.imageUrl != null && !carDetail.imageUrl.isEmpty()) {
            Picasso.with(context).load(carDetail.imageUrl).error(R.drawable.search).into(holder.image);
        }

    }

    @Override
    public int getItemCount() {
        return carDetails.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView title, price;
        ImageView image;

        public ViewHolder(View itemView) {
            super(itemView);

            title = itemView.findViewById(R.id.car_list_name);
            price = itemView.findViewById(R.id.car_price);
            image = itemView.findViewById(R.id.car_list_image);
        }
    }
}

